
/**
 * Copyright (C) 2018 Lukas Buchs
 * license https://github.com/lbuchs/WebAuthn/blob/master/LICENSE MIT
 */


        /**
         * checks a FIDO2 registration
         * @returns {undefined}
         */
        function checkregistration() {
            if (!window.fetch || !navigator.credentials || !navigator.credentials.create) {
                window.alert('This browser is not supported.');
                return;
            }
            // get default args
            window.fetch('/waserver?fn=getGetArgs', {method:'GET',cache:'no-cache'}).then(function(response) {
                return response.json();
                // convert base64 to arraybuffer
            }).then(function(json) {
                // error handling
                if (json.success === false) {
                    throw new Error(json.msg);
                }
                // replace binary base64 data with ArrayBuffer. a other way to do this
                // is the reviver function of JSON.parse()
                recursiveBase64StrToArrayBuffer(json);
                return json;
               // create credentials
            }).then(function(getCredentialArgs) {
                return navigator.credentials.get(getCredentialArgs);
                // convert to base64
            }).then(function(cred) {
                return {
                    id: cred.rawId ? arrayBufferToBase64(cred.rawId) : null,
                    clientDataJSON: cred.response.clientDataJSON  ? arrayBufferToBase64(cred.response.clientDataJSON) : null,
                    authenticatorData: cred.response.authenticatorData ? arrayBufferToBase64(cred.response.authenticatorData) : null,
                    signature : cred.response.signature ? arrayBufferToBase64(cred.response.signature) : null
                };
                // transfer to server
            }).then(JSON.stringify).then(function(AuthenticatorAttestationResponse) {
                return window.fetch('/waserver?fn=processGet', {method:'POST', body: AuthenticatorAttestationResponse, cache:'no-cache'});
                // convert to json
            }).then(function(response) {
                return response.json();
                // analyze response
            }).then(function(json) {
               if (json.success) {
                   window.alert(json.msg || 'Authorization Successful');
		   window.location.href="/webauthn";
               } else {
                   throw new Error(json.msg);
               }
               // catch errors
            }).catch(function(err) {
                window.alert(err.message || 'An unknown error has occured.');
            });
        }



