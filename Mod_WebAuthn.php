<?php

namespace Zotlabs\Module;

use Zotlabs\Lib\Apps;
use Zotlabs\Lib\Libsync;

class WebAuthn extends \Zotlabs\Web\Controller {

	function get()
	{


		$channel = \App::get_channel();
		$loc = $channel['channel_account_id'];
		$show_waform = true;
		$another_ok = true;
		$o = '';

		if (intval($loc)<1)
		{
			notice( t('Please log in.') . EOL );
                        goaway(z_root() . '/');
		}

/*
		if ($_REQUEST['xn']=='clean')
		{
			set_pconfig($loc,'webauthn','wakey_required',false);
			set_pconfig($loc,'webauthn','registrations',null);
		}
*/
		
		$keyvalid = false;
		$error_message = '';
		$is_logged_in = $this->check_webauthn_auth($loc);

		if ($is_logged_in && x($_REQUEST,'deletekey'))
		{
			$delkey = filter_var($_REQUEST['deletekey'],
						FILTER_SANITIZE_STRING);
			if ( strlen($delkey)>32 )
			{
				$serial_array = unserialize(get_pconfig($loc,'webauthn','registrations'));
				$new_serial = array();
				foreach ($serial_array as $k)
				{
					$reg = unserialize(base64_decode($k));
                                        if ($reg->credentialId != $delkey)
					{
						$new_serial[]=$k;
					}
				}
				if (count($new_serial)<1)
				{
					/* no longer using WebAuthn */
					$o = '<h1 style="color:Red;">WebAuthn 2FA DISABLED</h1>';
					$show_waform = false;
					$another_ok = false;
					/* delete everything */
					setcookie('wakeyauth','');
					set_pconfig($loc,'webauthn','wakey_required',false);
					set_pconfig($loc,'webauthn','approved','');
					set_pconfig($loc,'webauthn','await','');
					set_pconfig($loc,'webauthn','wakeyauth','');
					set_pconfig($loc,'webauthn','registrations',null);
					$_SESSION['wachallenge'] = null;
					setcookie('webauthn0k','');
					set_pconfig($loc,'webauthn','webauthn0k','');

				} else {
					$o = '<h1 style="color:Green;">Key Deleted</h1>';
					set_pconfig($loc,'webauthn','registrations',$new_serial);
				}
				$show_waform = false;
			} else {
				$o = '<h1 style="color:Red;">ERROR</h1>
					<p>Invalid WebAuthn Key</p>';
			}
		}

		if (x($_POST,'yk'))
		{
			$yk = filter_var($_POST['yk'],FILTER_SANITIZE_STRING);
			if ( (strlen($yk)>12) && (strlen($yk)<64) )
			{
				/* check key */
				//require_once 'Yubico.php';
				//$wakey = new Auth_Yubico(YUBIAPIUSER,YUBIAPIKEY);
				//$auth = $yubi->verify($yk);
				if ($auth) {
					if (!strstr($auth,'ERROR'))
					{
						/* this key is valid */
						/* check if new key */
						$serial_array = unserialize(get_pconfig($loc,'webauthn','registrations'));
						if (is_array($serial_array) && (count($serial_array)>0))
						{
							if ($serial_array[substr($yk,0,12)])
							{
								$keyvalid = true;
								$show_waform = false;
								$authkey = base64_encode(random_bytes(64));
								set_pconfig($loc,'webauthn','authkey',$authkey);
								setcookie('wakeyauth',$authkey);
								$o = '<h1>Authenticated</h1>
									<p>You have successfully authenticated</p>
									<p><a href="'.z_root().'/network">Continue to Network</a> &middot; 
										<a href="'.z_root().'/webauthn">Manage Keys</a></p>';
							} else {
								$error_message = '<div style="color:Red;padding:10px;">
                  <strong>WRONG KEY</strong></div>';
							}
						} else {
							/* NEW key */
							$serial = substr($yk,0,12);
							$keyvalid = true;
							$show_waform = false;
							$serial_array = array();
							$serial_array[$serial]=true;
							set_pconfig($loc,'webauthn','serial',serialize($serial_array));
							set_pconfig($loc,'webauthn','wakey_required',true);
							$authkey = base64_encode(random_bytes(64));
							set_pconfig($loc,'webauthn','authkey',$authkey);
							setcookie('wakeyauth',$authkey);

							$o = '<h1>Authenticated</h1>
								<p>You have successfully authenticated</p>
								<p><a href="'.z_root().'/network">Continue to Network</a> &middot;
									<a href="'.z_root().'/webauthn">Manage Keys</a></p>';
						}
					} else {
						$error_message = 
							'<div style="color:Red;padding:10px;">
								<strong>'.$auth.'</strong></div>';
					}
				}
			} 
		} else {
			/* additional key */
			if ($is_logged_in && x($_POST,'ayk'))
	                {
				if (get_pconfig($loc,'webauthn','wakey_required'))
				{
					$ayk = filter_var($_POST['ayk'],FILTER_SANITIZE_STRING);
					if ( (strlen($ayk)>12) && (strlen($ayk)<64) )
					{
						/* check key */
						//require_once 'Yubico.php';
						//$yubi = new Auth_Yubico(YUBIAPIUSER,YUBIAPIKEY);
						//$auth = $yubi->verify($ayk);
						if ($auth) {
							if (!strstr($auth,'ERROR'))
							{
								$serial = substr($ayk,0,12);
								$serial_array = unserialize(get_pconfig($loc,'webauthn','serial'));
								$serial_array[$serial]=true;
								set_pconfig($loc,'webauthn','serial',serialize($serial_array));
								$o = '<h1 style="color:Green;">Key Added</h1>
									<p>You have added another Auth Key.</p>';
								$show_waform = false;
							} else {
								$show_waform = false;	
								$o = '<h1 style="color:Red;">ERROR</h1>
									<p>There was an error adding your key: '.$auth.'</p>';
							}
						} else {
							$show_waform = false;
							$o = '<h1 style="color:Red;">ERROR</h1>
								<p>Invalid Operation 1</p>';
						}
					} else {
						$show_waform = false;
						$o = '<h1 style="color:Red;">ERROR</h1>
							<p>Invalid WebAuthn Key</p>';
					}
				} else {
					$show_waform = false;
					$o = '<h1 style="color:Red;">ERROR</h1>
						<p>Invalid Operation 2</p>';
				}
			}

			/* if they are logged in then let them add another key
				and delete them */
			if ($is_logged_in && $another_ok)
			{
				$show_waform = false;


				$registrations = get_pconfig($loc,'webauthn','registrations');
				$keys=array();
				if (is_array($registrations)) {
			                foreach ($registrations as $xreg) {
			                        $reg = unserialize(base64_decode($xreg));
			                        $keys[$reg->credentialId] = true;
			                }
				}

				$o .= '<h1>Manage WebAuthn 2FA</h1>
';
				/* check for await */
				$s_info = unserialize(get_pconfig($loc,'webauthn','await'));
				$show_s_info = false;
				if (is_array($s_info))
				{
					$check_approved = get_pconfig($loc,'webauthn','approved');
					if ($check_approved!='')
					{
						if ($s_info['ma']==$check_approved)
						{
							// already approved
						} else {
							$show_s_info = true;
						}
					} else {
						$show_s_info = true;
					}
					if ($show_s_info)
					{
						/* check for manual approve */
						if (x($_REQUEST,'approve'))
						{
							set_pconfig($loc,'webauthn','approved',$_REQUEST['approve']);
							set_pconfig($loc,'webauthn','await','');
								$o .= '<p><strong>Client Approved</strong></p>';
						} else {
							$o .= '<div style="padding:10px;border:2px solid #ccc;">
<p><strong>Awaiting Confirmation</strong></p>
<p>IP: '.htmlentities($s_info['ip'].' UA: '.$s_info['ua']).'<br>
<a href="/webauthn?approve='.urlencode($s_info['ma']).'">Approve (YES)</a></p>
</div>';
						}
					}
				}
		
$o .= '
<table border="1" cellspacing="0" cellpadding="3" width="500">
<tr><td><strong>Registration Id</strong></td><td><strong>Remove</strong></td></tr>
';
				foreach ($keys as $key=>$alive)
				{
					$o .= '<tr>
<td>'.base64_encode($key).'</td>
<td><a href="/webauthn?deletekey='.urlencode($key).'">Delete(x)</a></td>
</tr>
';
				}


				require_once('include/plugin.php');
                                head_add_js('/addon/webauthn/hda.js');
                                head_add_js('/addon/webauthn/thd.js');

				$o .= '</table>
<p><br></p>
<p><strong>Add Another WebAuthn Key</strong></p>
<button type="button" onclick="newregistration()">&#10133; Add New Registration</button>
';
			}


			$o .= '';
		}

		if ($show_waform)
		{

			if (!get_pconfig($loc,'webauthn','wakey_required'))
			{
				require_once('include/plugin.php');
				head_add_js('/addon/webauthn/hda.js');
				head_add_js('/addon/webauthn/thd.js');

        			$o = '<h1>WebAuthn 2FA</h1>
'.$error_message.'
<button type="button" onclick="newregistration()">&#10133; New Registration</button>
';
      			} else {
				require_once('include/plugin.php');
				head_add_js('/addon/webauthn/cha.js');
				head_add_js('/addon/webauthn/thd.js');

				$o = '<h1>WebAuthn 2FA</h1>
'.$error_message.'
<button type="button" onclick="checkregistration()">&#10068; Authorize Session</button>
';
			}
		}


$channel = \App::get_channel();
$username   = $channel['channel_name'];
$nickname   = $channel['channel_address'];

$o .= '
<p><br></p>
<p><strong>Note:</strong></p>
<p>This app supports FIDO2 / <strong>CTAP 1 (2FA) (Previously known as U2F)</strong>. We do not store information on your hardware key. (CTAP 2).
We use the certificate which is pre-stored on your device to create a signature. Usually your hardware vendor
will provide a software tool to regenerate the certifiates on your device.
Your device has the key to create the signature. We use this signature to verify
your login. We presume that the person with possession of your hardware device is authorized
to access your account. However, if your device is lost, stolen, or damaged you may <a href="/siteinfo">contact the
administrator of this web site</a>
and request a manual override.</p>
<p><strong>Important:</strong> The following information is used to create the signature,
which <em>may be personally identifiable information</em>. This information is gathered from the account information
you already provided on this web site.</p>
<p>Numeric Channel Id: '.$loc.'</p>
<p>Channel Id / Nickname: '.$nickname.'</p>
<p>Display Name: '.$username.'</p>

<p><strong>Supported Devices:</strong> 
Devices with a signed X.509 certificate. ECDAA and self attestation are not supported.</p>
<ul>
<li><a href="https://hypersecu.com/products/hyperfido">Hypersecu</a></li>
<li><a href="https://9to5google.com/2019/02/25/android-fido2-password-google/">Google Android Devices (Phones)</a></li>
<li><a href="https://cloud.google.com/titan-security-key/">Google Titan</a></li>
<li><a href="https://www.ftsafe.com/Products/FIDO2">Feitan</a></li>
<li><a href="https://www.yubico.com/solutions/fido2/">Yubico Yubikey</a></li>
<li><a href="https://shop.solokeys.com/">Solo Keys</a></li>
<li><a href="https://www.egistec.com/u2f-solution/">Egis</a></li>
<li><a href="http://www.e-wbm.com/">eWBM</a></li>
<li><a href="https://www.vasco.com/products/two-factor-authenticators/hardware/one-button/digipass-secureclick.html">DIGIPASS SecureClick</a></li>
</ul>
<p><strong>Supported Browsers:</strong> Firefox 60+, Chrome 67+, Opera 54+, Edge 18+</p>

<p text-align:center;"><img src="/addon/webauthn/lets-call-it-a-day.jpg" alt="Img" style="max-width:100%;height:auto;"></p>
<p>This software uses the WebAuthn PHP library by Lukas Buchs and Thomas Bleeker.</p>
';
		return $o;
	}

	/* consolidate auth check
		return true or false */
	function check_webauthn_auth($loc)
	{

		if (x($_COOKIE,'webauthn0k')&&(strlen($_COOKIE['webauthn0k'])>128))
		{
		  $chk_auth = get_pconfig($loc,'webauthn','webauthn0k');
		  if ($chk_auth == $_COOKIE['webauthn0k'])
		  {
		    return true;
		  }
		}


		if (x($_COOKIE,'wakeyauth')&&(strlen($_COOKIE['wakeyauth'])>64))
		{

			if ($_COOKIE['wakeyauth'] == get_pconfig($loc,'webauthn','authkey'))
			{
				return true;
			}
		}

		/* check remote */
		if (x($_COOKIE,'wamarrow'))
		{
			$chk_approved = get_pconfig($loc,'webauthn','approved');
			if ($chk_approved!='')
			{
				if ($_COOKIE['wamarrow']==$chk_approved)
				{
					return true;
				}
			}
		}
		return false;
	}

}

