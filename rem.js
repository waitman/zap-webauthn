
/**
 * Copyright (C) 2018 Lukas Buchs
 * license https://github.com/lbuchs/WebAuthn/blob/master/LICENSE MIT
 */

        function clearregistration() {
            window.fetch('/waserver?fn=clearRegistrations', {method:'GET',cache:'no-cache'}).then(function(response) {
                return response.json();
            }).then(function(json) {
               if (json.success) {
                   window.alert(json.msg);
               } else {
                   throw new Error(json.msg);
               }
            }).catch(function(err) {
                window.alert(err.message || 'unknown error occured');
            });
        }

