
/**
 * Copyright (C) 2018 Lukas Buchs
 * license https://github.com/lbuchs/WebAuthn/blob/master/LICENSE MIT
 */


        /**
         * creates a new FIDO2 registration
         * @returns {undefined}
         */
        function newregistration() {
            if (!window.fetch || !navigator.credentials || !navigator.credentials.create) {
                window.alert('Browser not supported.');
                return;
            }
            // get default args
            window.fetch('/waserver?fn=getCreateArgs', {method:'GET',cache:'no-cache'}).then(function(response) {
                return response.json();
                // convert base64 to arraybuffer
            }).then(function(json) {
                // error handling
                if (json.success === false) {
                    throw new Error(json.msg);
                }
                // replace binary base64 data with ArrayBuffer. a other way to do this
                // is the reviver function of JSON.parse()
                recursiveBase64StrToArrayBuffer(json);
                return json;
               // create credentials
            }).then(function(createCredentialArgs) {
                console.log(createCredentialArgs);
                return navigator.credentials.create(createCredentialArgs);
                // convert to base64
            }).then(function(cred) {
                return {
                    clientDataJSON: cred.response.clientDataJSON  ? arrayBufferToBase64(cred.response.clientDataJSON) : null,
                    attestationObject: cred.response.attestationObject ? arrayBufferToBase64(cred.response.attestationObject) : null
                };
                // transfer to server
            }).then(JSON.stringify).then(function(AuthenticatorAttestationResponse) {
                return window.fetch('/waserver?fn=processCreate', {method:'POST', body: AuthenticatorAttestationResponse, cache:'no-cache'});
                // convert to JSON
            }).then(function(response) {
                return response.json();
                // analyze response
            }).then(function(json) {
               if (json.success) {
                   window.alert(json.msg || 'registration success');
               } else {
                   throw new Error(json.msg);
               }
               // catch errors
            }).catch(function(err) {
                window.alert(err.message || 'unknown error occured');
            });
        }

