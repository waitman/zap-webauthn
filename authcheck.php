<?php

function check_webauthn()
{
	if ($_SERVER['REQUEST_URI']=='/webauthn')
	{
		return;
	}

	/* Allow them to get help */
	if ($_SERVER['REQUEST_URI']=='/siteinfo')
	{
		return;
	}


	$loc = intval(local_channel());
	$is_approved = false;

	/* is the user logged in? */
	if ($loc>0)			
	{

                /* did the user choose to require webauthna 2FA? */
                if (get_pconfig($loc,'webauthn','wakey_required'))
                {

			/* check remote approved */
			if (x($_COOKIE,'wamarrow'))
			{
				$chk_approved = get_pconfig($loc,'webauthn','approved');
				if ($chk_approved!='')
				{
					if ($_COOKIE['wamarrow']==$chk_approved)
					{
						$is_approved = true;
					}
				}
			}

			if (!$is_approved)
			{

				if (x($_COOKIE,'webauthn0k')&&(strlen($_COOKIE['webauthn0k'])>128))
				{
					$chk_auth = get_pconfig($loc,'webauthn','webauthn0k');
					if ($chk_auth == $_COOKIE['webauthn0k'])
					{
						$is_approved = true;
					}
				}
			}
	
			if (!$is_approved)
			{
				/* send the user to auth page */
				webauthn_gateway($loc);
				Header("Location: /webauthn");
				exit();
			}
		}
	} else {
		/*( they may have logged out )*/
		if (x($_COOKIE,'wamarrow'))
		{
			setcookie('wamarrow','');
		}
		if (x($_COOKIE,'webauthn0k'))
		{
			setcookie('webauthn0k','');
		}
	}
	return;
}

function webauthn_gateway($loc)
{
	/* we can authorize another account from  NEEDS CHANGED. the device should set a key, 
		but by permission of the primary
		an account that is already logged in
		example: approving a mobile device
		if you do not have the NFC device */
					
                if (x($_COOKIE,'wamarrow'))
                {
                        // the user is already registered
                } else {
                        $reg = base64_encode(random_bytes(64));
                        setcookie('wamarrow',$reg);
                        $s_info = array();
                        $s_info['ip']=filter_var($_SERVER['REMOTE_ADDR'],
                                                FILTER_SANITIZE_STRING);
                        $s_info['ua']=filter_var($_SERVER['HTTP_USER_AGENT'],
                                                FILTER_SANITIZE_STRING);
                        $s_info['ma']=$reg;
                        set_pconfig($loc,'webauthn','await',serialize($s_info));
                }
	return;
}
