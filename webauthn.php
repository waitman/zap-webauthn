<?php

/**
 * Name: WebAuthn App
 * Description: Provide 2FA using WebAuthn
 * Version: 1.0
 * MinVersion: 1.4.2
 * Author: Waitman Gobble <waitman@waitman.net>
 * Maintainer: Me
 */

use \Zotlabs\Extend\Route;
use \Zotlabs\Extend\Hook;

function webauthn_load() {
        Route::register('addon/webauthn/Mod_WebAuthn.php','webauthn');
	Route::register('addon/webauthn/server.php','waserver');
	Hook::register('build_pagehead', 'addon/webauthn/authcheck.php', 'check_webauthn');
}

function webauthn_unload() {
        Route::unregister_by_file('addon/webauthn/Mod_WebAuthn.php');
	Route::unregister_by_file('addon/webauthn/server.php');
	Hook::unregister_by_file('addon/webauthn/authcheck.php');
}

function webauthn_app_menu(&$b) {
	$b['app_menu'][] = '<div class="app-title"><a href="/webauthn">WebAuthn 2FA</a></div>'; 
}


