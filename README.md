
ZAP WebAuthn 2FA App

This app supports FIDO2 / CTAP 1 (2FA) (Previously known as U2F). We do not store information on your hardware key. (CTAP 2). We use the certificate which is pre-stored on your device to create a signature. Usually your hardware vendor will provide a software tool to regenerate the certifiates on your device. Your device has the key to create the signature. We use this signature to verify your login. We presume that the person with possession of your hardware device is authorized to access your account. However, if your device is lost, stolen, or damaged you may contact the administrator of this web site and request a manual override.


Supported Devices: Devices with a signed X.509 certificate. ECDAA and self attestation are not supported.

    Hypersecu
    Google Android Devices (Phones)
    Google Titan
    Feitan
    Yubico Yubikey
    Solo Keys
    Egis
    eWBM
    DIGIPASS SecureClick

Supported Browsers: Firefox 60+, Chrome 67+, Opera 54+, Edge 18+

This software uses the WebAuthn PHP library by Lukas Buchs and Thomas Bleeker.
