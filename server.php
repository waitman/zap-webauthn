<?php


/*
 * WebAuthn Code Copyright (C) 2018 Lukas Buchs
 * license https://github.com/lbuchs/WebAuthn/blob/master/LICENSE MIT
 * Some portions modified by Waitman Gobble <ns@waitman.net>
 * (See GH link for original source)
 */

namespace Zotlabs\Module;

define('HOSTNAME',$_SERVER['SERVER_NAME']); //possible security issue

class WaServer extends \Zotlabs\Web\Controller {

function get() {

session_start();


$channel = \App::get_channel();
$loc = $channel['channel_account_id'];
if ($loc<1) exit('not logged in.');
$username = $channel['channel_name'];
$nickname = $channel['channel_address'];


require_once('WebAuthn.php');

try {

    // read get argument and post body
    $fn = $_GET['fn'];
    $requireResidentKey = false; 

    $post = trim(file_get_contents('php://input'));
    if ($post) {
        $post = json_decode($post);
    }

    // Formats
    $formats = array();
    $formats[] = 'android-key';
    $formats[] = 'android-safetynet';
    $formats[] = 'fido-u2f';
    $formats[] = 'packed';

    // new Instance of the server library.
    // make sure that $rpId is the domain name.
    $WebAuthn = new \WebAuthn\WebAuthn('WebAuthn Library', HOSTNAME, $formats);
    $WebAuthn->addRootCertificates(__DIR__ . '/rootCertificates/solo.pem');
    $WebAuthn->addRootCertificates(__DIR__ . '/rootCertificates/yubico.pem');
    $WebAuthn->addRootCertificates(__DIR__ . '/rootCertificates/hypersecu.pem');
    $WebAuthn->addRootCertificates(__DIR__ . '/rootCertificates/globalSign.pem');
    $WebAuthn->addRootCertificates(__DIR__ . '/rootCertificates/googleHardware.pem');

    // ------------------------------------
    // request for create arguments
    // ------------------------------------

    if ($fn === 'getCreateArgs') {

        $createArgs = $WebAuthn->getCreateArgs($loc, $nickname, $username, 20, $requireResidentKey);
        print(json_encode($createArgs));
        // save challenge to session. you have to deliver it to processGet later.
        $_SESSION['wachallenge'] = base64_encode(serialize($WebAuthn->getChallenge()));

    // ------------------------------------
    // request for get arguments
    // ------------------------------------

    } else if ($fn === 'getGetArgs') {
        $ids = array();

	$registrations = get_pconfig($loc,'webauthn','registrations');
        if ($requireResidentKey) {

            if (!is_array($registrations) || count($registrations) === 0) {
		notice( t('We do not have any registrations in session to check the registration.') . EOL );
                //throw new \Exception('we do not have any registrations in session to check the registration');
		exit();
            }

        } else {
            // load registrations from session stored there by processCreate.
            // normaly you have to load the credential Id's for a username
            // from the database.
            if (is_array($registrations)) {
                foreach ($registrations as $xreg) {
			$reg = unserialize(base64_decode($xreg));
	                $ids[] = $reg->credentialId;
                }
            }

            if (count($ids) === 0) {
		notice( t('No registrations in the session.') . EOL );
                //throw new \Exception('no registrations in session.');
		exit();
            }
        }

        $getArgs = $WebAuthn->getGetArgs($ids);

        print(json_encode($getArgs));

        // save challange to session. you have to deliver it to processGet later.
        $_SESSION['wachallenge'] = base64_encode(serialize($WebAuthn->getChallenge()));

    // ------------------------------------
    // process create
    // ------------------------------------

    } else if ($fn === 'processCreate') {


$clientDataJSON = base64_decode($post->clientDataJSON);
$attestationObject = base64_decode($post->attestationObject);
$challenge = unserialize(base64_decode($_SESSION['wachallenge']));

$data = $WebAuthn->processCreate($clientDataJSON, $attestationObject, $challenge);
$registrations = get_pconfig($loc,'webauthn','registrations');
if (!is_array($registrations)) $registrations=array();
$registrations[]=base64_encode(serialize($data));
set_pconfig($loc,'webauthn','registrations',$registrations);
set_pconfig($loc,'webauthn','wakey_required',true);

$return = new \stdClass();
$return->success = true;
$return->msg = 'Registration Success.';
print(json_encode($return));

    // ------------------------------------
    // proccess get
    // ------------------------------------

    } else if ($fn === 'processGet') {
        $clientDataJSON = base64_decode($post->clientDataJSON);
        $authenticatorData = base64_decode($post->authenticatorData);
        $signature = base64_decode($post->signature);
        $id = base64_decode($post->id);
        $challenge = unserialize(base64_decode($_SESSION['wachallenge']));
        $credentialPublicKey = null;

        // looking up correspondending public key of the credential id
        // you should also validate that only ids of the given user name
        // are taken for the login.
	$registrations = get_pconfig($loc,'webauthn','registrations');
        if (is_array($registrations)) {
            foreach ($registrations as $xreg) {
		$reg = unserialize(base64_decode($xreg));
                if ($reg->credentialId === $id) {
                    $credentialPublicKey = $reg->credentialPublicKey;
                    break;
                }
            }
        }

        if ($credentialPublicKey === null) {
	    notice( t('Public Key for credential ID not found.') . EOL );
		setcookie('webauthn0k','');
		set_pconfig($loc,'webauthn','webauthn0k','');
            //throw new \Exception('Public Key for credential ID not found!');
		exit();
        }

        // process the get request. throws WebAuthnException if it fails
        $WebAuthn->processGet($clientDataJSON, $authenticatorData, $signature, $credentialPublicKey, $challenge);
        $return = new \stdClass();
        $return->success = true;

	/* here we set cookie to keep track of 2FA auth */
	$bco = base64_encode(random_bytes(128));
	setcookie('webauthn0k',$bco);
	set_pconfig($loc,'webauthn','webauthn0k',$bco);
	/* wipe out pending approvals */
	set_pconfig($loc,'webauthn','await','');

        print(json_encode($return));

    // ------------------------------------
    // process clear registrations
    // ------------------------------------

    } else if ($fn === 'clearRegistrations') {
	set_pconfig($loc,'webauthn','registrations',null);
        $_SESSION['wachallenge'] = null;
	setcookie('webauthn0k','');
	set_pconfig($loc,'webauthn','webauthn0k','');
        $return = new \stdClass();
        $return->success = true;
        $return->msg = 'all registrations deleted';
        print(json_encode($return));
    }

} catch (Throwable $ex) {
    $return = new \stdClass();
    $return->success = false;
    $return->msg = $ex->getMessage();
    print(json_encode($return));
}
if (x($_GET,'fn')) exit();
$o = '<h1>WaServer</h1>';
$o .= $post;
return ($o);
}

function post() {
	$this->get();
}

}
